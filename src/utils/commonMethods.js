

export const getBase64String = (file) => {

    return new Promise((resolve, reject) => {
        const fileReader = new FileReader();
        fileReader.readAsDataURL(file);
        fileReader.onload = () => {
            const base64 = fileReader.result;
            const encoded = base64.replace(`data:${file.type};base64,`, "").replace(" ", "+");
            const file_ext = file.type.replace("image/", "")
            if (['jpg', 'gif', 'png', 'jpeg'].includes(file_ext.toLowerCase())) {
                resolve(
                    {
                        encoded,
                        file_name: file.name,
                        file_ext: file_ext
                    }
                )
            } else {
                reject("Not a image file")
            }

        }

        fileReader.onerror = error => reject(error)
    })
}

