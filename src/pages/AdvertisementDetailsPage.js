import { Card, CardActionArea, CardContent, CardMedia, Container, IconButton, makeStyles, Paper, Typography } from '@material-ui/core'
import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import MainLayout from '../components/layout/MainLayout'
import { GET_ADVERTISEMENT_BY_ID_URL } from '../configs/ApiEndPoints'
import { ADVERTISEMENT_BASE_PATH } from '../configs/ImageEndPoint'

import ArrowLeftIcon from '@material-ui/icons/ArrowBack';
import { Fade } from 'react-awesome-reveal'

const AdvertisementDetailsPage = () => {
    let { id } = useParams();
    const classes = useStyles();
    const history = useHistory();
    const [advertisementData, setAdvertisementData] = useState({})
    useEffect(() => {
        Axios.get(`${GET_ADVERTISEMENT_BY_ID_URL}${id}`).then(response => {
            if (response && response.data && response.data.status) {
                const advertisement = response.data.content;
                setAdvertisementData(advertisement)
            }
        })
        return () => {
            setAdvertisementData({})
        }
    }, [])

    const categoryName = advertisementData && advertisementData.category && advertisementData.category.name && advertisementData.category.name;
    return (
        <MainLayout>
            <Container className={classes.container} maxWidth="md">
                {/* Back to advertisement list button */}
                <IconButton
                    className={classes.backButton}
                    onClick={() => { history.goBack() }}
                >
                    <ArrowLeftIcon color={"inherit"} />
                </IconButton>
                {/* Advertisement Details Card */}
                <Fade direction="up" triggerOnce>
                    <Card className={classes.root}>
                        <Paper elevation={4} >
                            <CardMedia
                                className={classes.media}
                                image={`${ADVERTISEMENT_BASE_PATH}${advertisementData.img_path}`}
                                title="Contemplative Reptile"
                                className={classes.media}
                            />
                            <CardContent>
                                {/* Category */}
                                <Typography className={classes.categoryName} gutterBottom variant="h6" component="p" > {categoryName} </Typography>

                                {/* Title */}
                                <Typography className={classes.title} gutterBottom variant="h4" component="h4">{advertisementData.title}</Typography>

                                {/* Description */}
                                <Typography className={classes.description} variant="body2" color="textSecondary" component="p">{advertisementData.description} </Typography>
                            </CardContent>
                        </Paper>
                    </Card>
                </Fade>

            </Container>
        </MainLayout>
    )
}

export default AdvertisementDetailsPage

const useStyles = makeStyles({
    container: {
        position: "relative"
    },
    backButton: {
        position: "absolute", top: "5px", left: "-15px", backgroundColor: "#fff", zIndex: "100"
    },
    root: {
        marginTop: "25px",
        marginLeft: "10px",
        marginRight: "10px",
        marginBottom: "20px", position: "relative",
        borderTopLeftRadius: "15px", borderTopRightRadius: "15px"
    },
    media: { height: "500px", borderTopLeftRadius: "15px", borderTopRightRadius: "15px" },
    categoryName: { textTransform: "uppercase", fontSize: "10px" },
    title: { textTransform: "capitalize" },
    description: { color: "rgb(91,91,91)", fontSize: "18px", whiteSpace: "pre-line" },
});
