import React from 'react'
import AdvertisementComponent from '../components/advertisement/AdvertisementComponent'
import MainLayout from '../components/layout/MainLayout'

const AdvertisementPage = () => {
    return (
        <MainLayout>
            <AdvertisementComponent />
        </MainLayout>
    )
}

export default AdvertisementPage
