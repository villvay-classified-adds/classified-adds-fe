import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import purple from '@material-ui/core/colors/purple';
import { AdvertisementContext } from '../../contexts/AdvertisementContext';
import { useHistory } from 'react-router-dom';



const MainLayout = ({ children }) => {
    const classes = useStyles();
    const history = useHistory();
    const { favorite_only, setFavoriteOnly } = useContext(AdvertisementContext);
    return (
        <div className={classes.root}>
            <AppBar position="static" color="default">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>Classified Ads</Typography>
                    <Button
                        id="home-btn"
                        className={favorite_only ? classes.menuButton : classes.activeMenuButton}
                        onClick={() => { setFavoriteOnly(false); history.push('/') }}
                    >Home</Button>
                    <Button
                        id="fav-btn"
                        className={!favorite_only ? classes.menuButton : classes.activeMenuButton}
                        onClick={() => { setFavoriteOnly(true); history.push('/') }}
                    >Favorites</Button>
                </Toolbar>
            </AppBar>

            { children}
        </div >
    )
}

export default MainLayout

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    title: {
        flexGrow: 1,
    },
    activeMenuButton: {
        minHeight: "64px",
        borderBottom: `2px solid ${purple[500]}`,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
        color: purple[500]
    },
    menuButton: {
        minHeight: "64px",
        borderBottom: "1px solid transparent",
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
        // color: purple[500]
    }
}));
