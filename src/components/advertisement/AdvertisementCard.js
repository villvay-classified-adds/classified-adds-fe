import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { ADVERTISEMENT_BASE_PATH } from '../../configs/ImageEndPoint';
import { IconButton } from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { useHistory } from 'react-router-dom';



export default function AdvertisementCard({ data, ToggleFavoritesCallback }) {
    const history = useHistory();
    const classes = useStyles();
    const description = `${data.description.substring(0, 250)}${data.description.length > 250 && "..."}`
    const is_fav = parseInt(data.is_fav);

    const handleCardOnClick = (id) => history.push(`/advertisement/${id}`);
    const handleToggleFavorite = () => ToggleFavoritesCallback(data.id, is_fav === 1 ? 0 : 1)
    return (
        <Card className={classes.root}>
            {/* Favorite Icon */}
            <IconButton
                className={classes.iconButtonStyles}
                onClick={handleToggleFavorite}
            >
                <FavoriteIcon color={is_fav === 1 ? "primary" : "inherit"} />
            </IconButton>
            <CardActionArea onClick={() => handleCardOnClick(data.id)}>
                {/* Advertisement Image */}
                <CardMedia
                    className={classes.media}
                    image={`${ADVERTISEMENT_BASE_PATH}${data.img_path}`}
                    title={data.title}
                />
                <CardContent>
                    <Typography className={classes.categoryLabel} gutterBottom variant="h6" component="p" >{data.category.name}</Typography>
                    <Typography gutterBottom variant="h6" component="h6">{data.title}</Typography>
                    <Typography variant="body2" color="textSecondary" component="p">{description} </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
        minWidth: "22vw",
        marginLeft: "10px",
        marginRight: "10px",
        marginBottom: "20px", position: "relative"
    },
    iconButtonStyles: { position: "absolute", top: "5px", right: "5px", backgroundColor: "#fff", zIndex: "100" },
    media: {
        height: "250px"
    },
    categoryLabel: { textTransform: "uppercase", fontSize: "10px" },
});