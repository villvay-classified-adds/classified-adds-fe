import React, { useState, useEffect, useContext } from 'react'
import Drawer from '@material-ui/core/Drawer';
import NewAdvertisementForm from './NewAdvertisementForm';
import { AdvertisementContext } from '../../contexts/AdvertisementContext';
import Axios from 'axios';
import { GET_ADVERTISEMENTS_URL, GET_CATEGORIES_URL, PUT_TOGGLE_FAVORITES_URL } from '../../configs/ApiEndPoints';
import { Button, FormControl, InputLabel, makeStyles, Select } from '@material-ui/core';
import AdvertisementCard from './AdvertisementCard';
import AdvertisementCardListComponent from './AdvertisementCardListComponent';

const AdvertisementComponent = () => {
    const { categories, setCategories, setAdvertisements } = useContext(AdvertisementContext)
    const [drawerOpen, setDrawerOpen] = useState(false);
    const [activeCategoryId, setActiveCategoryId] = useState(0);
    const classes = useStyles();

    //#region Handling Methods
    /**
     * Toggle between new advertisement form that slides from right side.
     */
    const handleDrawerClose = () => {
        setDrawerOpen(false)
    }

    /**
     * Get Categories from API and set it into context
     */
    const getCategories = () => {
        Axios.get(GET_CATEGORIES_URL).then(response => {
            if (response && response.data && response.data.status) {
                const categoriesList = response.data.content || [];
                setCategories(categoriesList)
            }
        })
    }

    /**
     * Get Advertisements list from API and set it  into context.
     */
    const getAdvertisements = () => {
        Axios.get(GET_ADVERTISEMENTS_URL).then(response => {
            if (response && response.data && response.data.status) {
                const advertisementsList = response.data.content || [];
                setAdvertisements(advertisementsList)
            }
        })
    }

    /**
     * Mark as Favorite advertisement.
     * @param {number} id Advertisement ID
     * @param {number} is_fav is favorite tiny int
     */
    const toggleFavorites = (id, is_fav) => {
        Axios.put(PUT_TOGGLE_FAVORITES_URL, { id, is_fav }).then(response => {
            if (response && response.data && response.data.status) {
                setAdvertisements(prevAdvertisements => {
                    return prevAdvertisements.map(prevAdds => ({ ...prevAdds, is_fav: parseInt(prevAdds.id) === parseInt(id) ? is_fav : prevAdds.is_fav }))
                })
            }
        })
    }
    //#endregion Handling Methods


    useEffect(() => {
        // Getting Categories at the start up
        getCategories()
        // Getting Advertisements at the start up
        getAdvertisements()
    }, [])
    return (
        <div className={classes.container}>
            <div className={classes.buttonWrapper}>
                {/* Select Categories input */}
                <div className={classes.selectWrapper}>
                    <FormControl variant="filled" >
                        <Select native value={activeCategoryId} onChange={event => setActiveCategoryId(parseInt(event.target.value))} placeholder="Category" >
                            <option value="0" >All</option>>
                            {categories.map(category => <option key={category.id} value={category.id}>{category.name}</option>)}
                        </Select>
                    </FormControl>
                </div>
                {/* New Classified Button */}
                <div className={classes.saveButtonWrapper}>
                    <Button className={classes.saveButton} variant="contained" color="primary" onClick={() => setDrawerOpen(true)}>New Classified</Button>
                </div>
            </div>
            {/* Advertisement List Component that render the filtered advertisements cards */}
            <AdvertisementCardListComponent activeCategory={activeCategoryId} ToggleFavorites={toggleFavorites} />

            {/* Make New Advertisement Form Drawer */}
            <Drawer anchor="right" open={drawerOpen} onClose={handleDrawerClose}>
                <NewAdvertisementForm categoriesList={categories} OnCloseClick={handleDrawerClose} OnFormSubmit={setAdvertisements} />
            </Drawer>
        </div>
    )
}

export default AdvertisementComponent;

const useStyles = makeStyles((theme) => ({
    container: {
        backgroundColor: "#f8f8f8"
    },
    buttonWrapper: {
        height: "70px", display: "flex", flex: "1", padding: "10px 20px"
    },
    selectWrapper: {
        display: "flex", flex: "1", flexDirection: "row", alignItems: "center", justifyContent: "flex-start"
    },
    saveButtonWrapper: {
        display: "flex", flex: "1", flexDirection: "row", alignItems: "center", justifyContent: "flex-end"
    },
    saveButton: {
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        borderBottomRightRadius: 25,
        borderBottomLeftRadius: 25,
    }
}));

