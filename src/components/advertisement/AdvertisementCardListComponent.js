import { makeStyles } from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react'
import { Fade } from "react-awesome-reveal";
import { AdvertisementContext } from '../../contexts/AdvertisementContext'
import AdvertisementCard from './AdvertisementCard'

const AdvertisementCardListComponent = ({ activeCategory, ToggleFavorites }) => {
    const classes = useStyles();
    const { advertisements, favorite_only } = useContext(AdvertisementContext);
    const [filteredAdvertisements, setFilteredAdvertisements] = useState([])

    useEffect(() => {
        // ! FILTERING THE ADVERTISEMENT BY SELECTED category or favorite_only state
        let tempAdvertisements = advertisements;
        if (parseInt(activeCategory) !== 0) {
            tempAdvertisements = tempAdvertisements.filter(adv => parseInt(adv.category_id) === parseInt(activeCategory))
        }
        if (favorite_only) {
            tempAdvertisements = tempAdvertisements.filter(adv => parseInt(adv.is_fav) === 1)
        }
        setFilteredAdvertisements(tempAdvertisements)
        return () => {
            setFilteredAdvertisements([])
        }
    }, [advertisements, favorite_only, activeCategory])
    return (
        <div className={classes.container}>
            {filteredAdvertisements.map(
                advertisement => (
                    <Fade key={advertisement.id} direction="up" triggerOnce>
                        <AdvertisementCard data={advertisement} ToggleFavoritesCallback={ToggleFavorites} />
                    </Fade>)
            )}
        </div>
    )
}

export default AdvertisementCardListComponent


const useStyles = makeStyles((theme) => ({
    container: {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        padding: "20px", justifyContent: "flex-start"
    }
}));
