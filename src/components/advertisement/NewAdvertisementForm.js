import { Button, FormControl, IconButton, makeStyles, Select, TextField, Typography } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import React, { useState } from 'react'
import validator from 'validator';
import { getBase64String } from '../../utils/commonMethods';
import { NotificationManager } from 'react-notifications';
import { POST_CREATE_ADVERTISEMENT_URL } from '../../configs/ApiEndPoints'
import Axios from 'axios';
const NewAdvertisementForm = ({ categoriesList, OnCloseClick, OnFormSubmit }) => {
    const classes = useStyles();

    // Create new advertisement form data
    const [formData, setFormData] = useState({
        title: "",
        description: "",
        category_id: null,
        file: null
    })

    //#region Handling Methods

    /** Get uploaded file into the base64 format. 
     * file type validation goes here.
     * @param {object} event event object
     */
    const handleFileOnChange = event => {
        getBase64String(event.target.files[0]).then(
            file => setFormData(prvFormData => ({ ...prvFormData, file }))
        ).catch(error => NotificationManager.warning('Please Upload an Image file', 'Form Error'));
    }
    /**
     * Handle Form Submission
     * Validations goes here
     */
    const handleOnSubmit = () => {
        // destructuring formData object
        const { title, description, category_id, file: advertisement_img } = formData

        // Checking validations
        const titleValidationFailed = validator.isEmpty(title);
        const descriptionValidationFailed = validator.isEmpty(description);
        const categoryIdValidationFailed = category_id === null
        const fileValidationFailed = advertisement_img === null;

        if (titleValidationFailed || descriptionValidationFailed || categoryIdValidationFailed || fileValidationFailed) {
            // Validation Failed
            if (titleValidationFailed) { NotificationManager.error('Please add Advertisement Title', 'Form Error') }
            if (descriptionValidationFailed) { NotificationManager.error('Please add Advertisement Description', 'Form Error') }
            if (categoryIdValidationFailed) { NotificationManager.error('Please Select a Category', 'Form Error') }
            if (fileValidationFailed) { NotificationManager.error('Please Upload an image', 'Form Error') }
        }
        else {
            // Making payload to create new advertisement.
            const payload = {
                title, description, category_id, advertisement_img, is_fav: "0"
            }

            // Calling POST API to insert data
            Axios.post(POST_CREATE_ADVERTISEMENT_URL, payload).then(response => {
                if (response && response.data && response.data.status) {
                    // Getting inserted advertisement and push it into the context
                    const advertisement = response.data.content;
                    OnFormSubmit(prevAdvertisements => ([...prevAdvertisements, advertisement]))

                    // Success message.
                    NotificationManager.success('Create new advertisement succeeded', 'Yeah..')

                    // Reset the form.
                    setFormData({
                        title: "",
                        description: "",
                        category_id: null,
                        file: null
                    })

                    // Close Drawer
                    OnCloseClick()
                }
                else {
                    NotificationManager.error('Create new advertisement failed', 'Error')
                }
            })
        }
    }
    //#endregion Handling Methods

    return (
        <div className={classes.formContainer}>
            <div className={classes.formWrapper}>
                {/* Close Icon Wrapper */}
                <div className={classes.closeIconWrapper}>
                    <IconButton aria-label="delete" size="large" onClick={() => OnCloseClick()}>
                        <CloseIcon />
                    </IconButton>
                </div>

                {/* Form Title Wrapper */}
                <div className={classes.formTitleWrapper}>
                    <Typography variant="h5" gutterBottom>New Classified</Typography>
                </div>

                {/* Title Text Input Wrapper */}
                <div className={classes.textInputWrapper}>
                    <TextField id="filled-basic" label="Title" variant="filled" fullWidth
                        onChange={(event => setFormData(prevFormData => ({ ...prevFormData, title: event.target.value })))}
                        value={formData.title}
                    />
                </div>

                {/* Select Input Wrapper */}
                <div className={classes.selectWrapper}>
                    <FormControl variant="filled" fullWidth>
                        <Select
                            native
                            value={formData.category_id}
                            onChange={(
                                event => setFormData(
                                    prevFormData => (
                                        { ...prevFormData, category_id: event.target.value }
                                    )
                                )
                            )}
                            inputProps={{
                                name: 'age',
                                id: 'filled-age-native-simple',
                            }}
                            placeholder="Category"
                        >
                            <option value="0" >Category</option>>
                            {categoriesList.map(category => <option key={category.id} value={category.id}>{category.name}</option>)}
                        </Select>
                    </FormControl>
                </div>

                {/* Description Text Area Wrapper */}
                <div className={classes.textAreaWrapper}>
                    <TextField
                        id="filled-multiline-static"
                        label="Description"
                        multiline
                        rows={10}
                        defaultValue=""
                        variant="filled" fullWidth
                        onChange={(event => setFormData(prevFormData => ({ ...prevFormData, description: event.target.value })))}
                        value={formData.description}
                    />
                </div>

                {/* Upload Image Input Field Wrapper */}
                <div className={classes.uploadImageWrapper}>
                    <Button
                        color="primary"
                        component="label"
                    >
                        Upload Photo
                        <input
                            type="file"
                            style={{ display: "none" }}
                            onChange={handleFileOnChange}
                        />
                    </Button>
                </div>

                {/* Submit Button Wrapper */}
                <div className={classes.submitWrapper}>
                    <Button variant="contained" color="primary" onClick={handleOnSubmit}
                        style={
                            {
                                borderTopLeftRadius: 25,
                                borderTopRightRadius: 25,
                                borderBottomRightRadius: 25,
                                borderBottomLeftRadius: 25,
                            }}
                    >Save and Publish</Button>
                </div>
            </div>
        </div>
    )
}

export default NewAdvertisementForm

const useStyles = makeStyles((theme) => ({
    formContainer: {
        width: "25vw",
        backgroundColor: "#fff",
        padding: "25px",
        height: "100%",

    },
    formWrapper: {
        display: "flex",
        height: "100%",
        flex: 1,
        flexDirection: 'column'
    },
    closeIconWrapper: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-end",
    },
    formTitleWrapper: {
        flex: 2,
    },
    textInputWrapper: {
        flex: 3,
    },
    selectWrapper: {
        flex: 3,
    },
    textAreaWrapper: {
        flex: 9,
    },
    uploadImageWrapper: {
        flex: 2,
    },
    submitWrapper: {
        flex: 2,
    },
}));
