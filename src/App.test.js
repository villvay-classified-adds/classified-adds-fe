import App from './App';
import { shallow } from 'enzyme'

describe("Classified APP Testing", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<App />)
  })

  it('should render AdvertisementPage Component', () => {
    const advertisementPage = wrapper.find('AdvertisementPage');
    expect(advertisementPage.length).toEqual(1)
  })
  it('should render AdvertisementDetailsPage Component', () => {
    const advertisementDetailsPage = wrapper.find('AdvertisementDetailsPage');
    expect(advertisementDetailsPage.length).toEqual(1)
  })
})