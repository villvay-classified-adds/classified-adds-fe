const BasePath = "http://127.0.0.1:8000/api";

export const GET_CATEGORIES_URL = `${BasePath}/category/get-all`
export const GET_ADVERTISEMENTS_URL = `${BasePath}/advertisement/get-all`
export const GET_ADVERTISEMENT_BY_ID_URL = `${BasePath}/advertisement/get-by-id?id=`
export const POST_CREATE_ADVERTISEMENT_URL = `${BasePath}/advertisement/create`
export const PUT_TOGGLE_FAVORITES_URL = `${BasePath}/advertisement/toggleFav`