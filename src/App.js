import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import AdvertisementContextProvider from "./contexts/AdvertisementContext";
import AdvertisementDetailsPage from "./pages/AdvertisementDetailsPage";
import AdvertisementPage from "./pages/AdvertisementPage";
import { NotificationContainer } from 'react-notifications';
function App() {
  return (
    <AdvertisementContextProvider>
      <Router>
        <Switch>
          <Route path="/advertisement/:id">
            <AdvertisementDetailsPage />
          </Route>
          <Route path="/">
            <AdvertisementPage />
          </Route>
        </Switch>
      </Router>
      <NotificationContainer />

    </AdvertisementContextProvider>
  );
}

export default App;
