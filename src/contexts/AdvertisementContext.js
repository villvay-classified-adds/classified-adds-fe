import React, { createContext, useState } from 'react';

export const AdvertisementContext = createContext()

const AdvertisementContextProvider = ({ children }) => {
    const [advertisements, setAdvertisements] = useState([])
    const [categories, setCategories] = useState([])
    const [favorite_only, setFavoriteOnly] = useState(false)

    return (
        <AdvertisementContext.Provider
            value={{
                advertisements,
                categories,
                favorite_only,
                setAdvertisements,
                setCategories,
                setFavoriteOnly
            }}
        >
            {children}
        </AdvertisementContext.Provider>
    )
}

export default AdvertisementContextProvider;